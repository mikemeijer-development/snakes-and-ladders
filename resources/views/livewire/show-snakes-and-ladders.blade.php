<div>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Snakes and Ladders') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">


            @if ($totalThrows)
                <div class="grid grid-cols-4">
                    <div>
                        <p>{{ __('Total') }}: {{ $total }}</p>
                    </div>

                    <div>
                        <ul>
                            @foreach ($dices as $dice)
                                <li>{{ __('Dice') }} {{ $loop->index }}: {{ $dice }}</li>
                            @endforeach
                        </ul>
                    </div>

                    <div>
                        <p>{{ __('Total Throws') }}: {{ $totalThrows }}</p>
                    </div>

                    <div>
                        <p>{{ __('Total of Throw') }}: {{ $totalOfThrow }}</p>
                    </div>
                </div>
            @endif

            <div class="flex justify-between">
                <div>
                    @if ($message)
                        <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative"
                             role="alert">
                            <strong class="font-bold">Holy smokes!</strong>
                            <span class="block sm:inline"> {{ $message }}</span>
                        </div>
                    @endif
                </div>

                @if ($won)
                    <div>
                        <button class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
                                wire:click="resetGame">
                            {{ __('Reset Game') }}
                        </button>
                    </div>
                @endif

                @if (!$won)
                    <div>
                        <button class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
                                wire:click="play">
                            {{ __('Throw Dice') }}
                        </button>
                    </div>
                @endif
            </div>


        </div>
    </div>
</div>
