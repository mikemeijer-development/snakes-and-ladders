<?php

namespace App\Enums;

enum Obstacle
{
    case Snake;
    case Ladder;
}
