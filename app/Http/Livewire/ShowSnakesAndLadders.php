<?php

namespace App\Http\Livewire;

use App\Enums\Obstacle;
use Illuminate\Support\Arr;
use Livewire\Component;

class ShowSnakesAndLadders extends Component
{

    public int   $total         = 0;
    public int   $amountOfDices = 2;
    public int   $totalOfThrow  = 0;
    public int   $totalThrows   = 0;
    public array $dices         = [];

    public int $diceMin = 1;
    public int $diceMax = 6;

    public int $minBox = 1;
    public int $maxBox = 36;

    public ?string $message = null;

    public bool $won = false;

    protected array $obstacles = [
        [
            'type'  => Obstacle::Snake,
            'box'   => 12,
            'steps' => 10
        ],
        [
            'type'  => Obstacle::Ladder,
            'box'   => 3,
            'steps' => 13
        ],
        [
            'type'  => Obstacle::Ladder,
            'box'   => 15,
            'steps' => 10
        ],
        [
            'type'  => Obstacle::Snake,
            'box'   => 30,
            'steps' => 26
        ],
        [
            'type'  => Obstacle::Ladder,
            'box'   => 21,
            'steps' => 11
        ],
        [
            'type'  => Obstacle::Snake,
            'box'   => 35,
            'steps' => 13
        ],
    ];


    public function mount()
    {

    }


    public function throwDice()
    {
        $newTotalOfThrow = 0;

        for ($x = 0; $x <= $this->amountOfDices; $x++) {
            $newTotalOfThrow += rand($this->diceMin, $this->diceMax);
        }

        $this->totalOfThrow = $newTotalOfThrow;

        $newTotal = $this->total + $this->totalOfThrow;

        $this->total = $this->checkForObstacles($newTotal);
    }


    /**
     * Check for obstacle and return new value if needed.
     *
     * @param int $total
     *
     * @return int
     */
    public function checkForObstacles(int $total): int
    {
        $obstacle = Arr::first($this->obstacles, fn($value) => $value['box'] === $total);

        if ($obstacle && $obstacle['type'] === Obstacle::Ladder) {
            $steps = $obstacle['steps'];
            $this->message = "You climb the lader for $steps steps.";
            $total += $steps;
        }

        if ($obstacle && $obstacle['type'] === Obstacle::Snake) {
            $steps = $obstacle['steps'];
            $this->message = "You hit a snake and go back for $steps steps.";
            $total -= $steps;
        }

        if ($total >= $this->maxBox) {
            $this->message = 'You won!';
            $this->won = true;
        }

        return $total;
    }


    public function resetGame()
    {
        $this->totalThrows = 0;
        $this->totalOfThrow = 0;
        $this->total = 0;
        $this->message = null;
        $this->won = false;
    }

    public function play()
    {
        $this->totalThrows++;

        $this->throwDice();
    }


    public function render()
    {
        return view('livewire.show-snakes-and-ladders');
    }
}
